<?php
namespace app\models;
/**
 * Created by PhpStorm.
 * User: panda
 * Date: 03.08.18
 * Time: 9:10
 */

use Yii;
use yii\base\Model;
use app\models\Ad;
/**
 * Class AdForm
 */
class AdForm extends Model
{
    public $user_id;
    public $title;
    public $description;

    public function rules()
    {
        return [
            [['title','description'], 'required'],
            ['title', 'string', 'min' => 5, 'max' => 100]
        ];
    }

    public function saveAd()
    {
        if (!$this->validate()) {
            return null;
        }

        $ad = new Ad();
        $ad->user_id = Yii::$app->user->id;
        $ad->title = $this->title;
        $ad->description = $this->description;

        return $ad->save() ? $ad : null;
    }
}