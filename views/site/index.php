<?php

use yii\widgets\LinkPager;
use yii\web\UrlManager;
/* @var $this yii\web\View */

$this->title = 'Main page';
?>
<div class="container">
    <?php foreach ($models as $model): ?>
        <div class="row">
            <div class="panel">
                <div class="panel-heading d-flex justify-content-between bg-info panel-danger">
                    <div class="row">
                        <div class="col-lg-3">
                            <a href="<?=\yii\helpers\Url::toRoute(['site/view', 'id' => $model['id']]) ?>" style="text-decoration: none; color: #000000;"><h3 class="p-2 text-left"><?= $model['title'] ?></h3></a>
                        </div>
                        <div class="col-lg-offset-6 col-lg-3">
                            <p class="text-right" style="font-size: 15px;"><?= $model['username'] ?></p>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <p style="font-size: 16px;"><?= $model['description'] ?></p>
                </div>
                <?php if ($model['user_id'] == Yii::$app->user->id): ?>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-1">
                                <a href="<?=\yii\helpers\Url::toRoute(['/site/view', 'id' => $model['id']]) ?>"><div class="fa fa-eye" href="" style="font-size: 14px; color: #4cae4c;"></div></a>
                                <a href="<?=\yii\helpers\Url::toRoute(['/site/delete', 'id' => $model['id']]) ?>"><div class="fa fa-trash-alt" href="" style="font-size: 14px; color: #bd2130;"></div></a>
                                <a href="<?=\yii\helpers\Url::toRoute(['/site/edit', 'id' => $model['id']]) ?>"><div class="fa fa-edit" style="font-size: 14px;"></div></a>
                            </div>
                            <div class="col-lg-offset-9 col-lg-2 ">
                                <em><p class="text-right" style="font-size: 12px;"><?= date('Y-m-d H:i:s', $model['created_at']); ?></p></em>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-1">
                                <a href="<?=\yii\helpers\Url::toRoute(['/site/view', 'id' => $model['id']]) ?>"><div class="fa fa-eye" href="" style="font-size: 14px; color: #4cae4c;"></div></a>
                            </div>
                            <div class="col-lg-offset-9 col-lg-2 ">
                                <em><p class="text-right" style="font-size: 12px;"><?= date('Y-m-d H:i:s', $model['created_at']); ?></p></em>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>

    <?= LinkPager::widget([
        'pagination' => $pages,
    ]); ?>
</div>
