<?php
/**
 * Created by PhpStorm.
 * User: panda
 * Date: 03.08.18
 * Time: 21:24
 */
$this->title = $ad->title;
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <div class="col-lg-12">
            <h2><?= $ad->title; ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p><?= $ad->description; ?></p>
        </div>
    </div>

    <p class="text-right">Author: <b><?= $ad->user->username; ?></b></p>
    <p class="text-right">Date: <b><?= date('Y-m-d H:i:s', $ad->created_at); ?></b></p>
<?php if ($ad->user->id === Yii::$app->user->id): ?>
    <a href="<?=\yii\helpers\Url::toRoute(['/site/delete', 'id' => $ad->id]) ?>"><div class="fa fa-trash-alt" href="" style="font-size: 14px; color: #bd2130;"></div></a>
    <a href="<?=\yii\helpers\Url::toRoute(['/site/edit', 'id' => $ad->id]) ?>"><div class="fa fa-edit" style="font-size: 14px;"></div></a>
<?php endif; ?>