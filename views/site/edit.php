<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create Ad';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-ad">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-offset-3 col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'form-ad']); ?>
            <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <div class="form-group">
                <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'ad-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>