<?php

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Main', 'url' => ['/site/index']],
        ['label' => 'Create Ad', 'url' => ['site/edit'], 'visible' => !Yii::$app->user->isGuest],
        Yii::$app->user->isGuest ?
            (
            ['label' => 'Log In', 'url' => ['/site/login']]
            )
            :
            (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Log Out (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
    ],
]);
NavBar::end();
?>
