<?php

namespace app\controllers;

use app\models\AdForm;
use app\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Ad;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = Ad::find()
            ->select([Ad::tableName().'.id', 'title', 'description','user_id',Ad::tableName().'.created_at',User::tableName().'.username'])
            ->leftJoin(User::tableName(), User::tableName().'.id = '.Ad::tableName().'.user_id')
            ->orderBy([Ad::tableName().'.created_at' => SORT_DESC])
            ->asArray();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
        ]);

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()){
                return $this->goBack();
            } elseif (is_null(User::findByUsername($model->username))) {
                if($this->signup($model)) {
                    return $this->goHome();
                }
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Singup action
     *
     * @return Response|string
     */
    private function signup($model)
    {
        $model->username = trim($model->username);
        $model->password = trim($model->password);
        if ($model->username && $model->password) {
            if (iconv_strlen($model->password)>=6) {
                $user = new User();
                $user->username = $model->username;
                $user->setPassword($model->password);
                $user->generateAuthKey();
                $user->save();
                Yii::$app->user->login($user);
                return true;
            }
        }

        return false;
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionDelete($id)
    {
        $ad = Ad::findOne($id);
        $ad->delete();
        return $this->goHome();
    }

    /**
     * View Ad
     * @param $ad_id
     * @return string
     */
    public function actionView($id)
    {
        $ad = Ad::find()
            ->where(['id' => $id])
            ->one();
        return $this->render('view', [
            'ad' => $ad,
        ]);
    }


    public function actionEdit($id = null)
    {
        $model = new AdForm();

        if (!is_null($id)){
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->actionView($model->id);
                }

            }
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($ad = $model->saveAd()) {
                return $this->actionView($ad->id);
            }
        }
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Ad::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
